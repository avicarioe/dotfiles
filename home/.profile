#
# ~/.profile
#

export EDITOR=vim
export TERMINAL=termite
export BROWSER=firefox

[[ -f ~/.bashrc ]] && . ~/.bashrc
