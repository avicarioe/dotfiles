#!/bin/bash

FOLDER=$HOME
DATE=$(date +%d-%m-%Y)

for i in {0..99}; do
	FILE=$(printf "%s/%s_%02d_scrot.png" $FOLDER $DATE $i)
	if [ ! -e $FILE ]; then
		scrot "$FILE" "$@"
		break
	fi
done


