#!/bin/bash

CURR=$(setxkbmap -query | grep layout | sed "s/layout:\s*\(.*\)/\1/")
 
if [[ $CURR = "se" ]]
then
	CH="es,es"
else
	CH="se"
fi

setxkbmap -layout $CH
notify-send "Change layout" $CH
