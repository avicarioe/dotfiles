set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'Valloric/YouCompleteMe'

call vundle#end()
filetype plugin indent on

let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:ycm_enable_diagnostic_highlighting = 0

if !has('nvim')
	source $VIMRUNTIME/defaults.vim
endif

set background=dark
set number
set relativenumber
hi LineNr ctermfg=grey

set tabstop=4
set shiftwidth=4
set noexpandtab
autocmd FileType python setlocal tabstop=4 noexpandtab

""hi Visual term=reverse cterm=reverse

if filereadable('.vimlocal')
	source .vimlocal
endif

set colorcolumn=80
set ignorecase

set listchars=eol:¬,tab:»\ ,trail:.
set list
hi SpecialKey ctermfg=DarkBlue
hi NonText ctermfg=DarkBlue
