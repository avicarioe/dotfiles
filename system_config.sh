#!/bin/bash

#TODO check previous

sed -i '/es_ES.UTF-8/s/^#//g' /etc/locale.gen
locale-gen
echo LANG=es_ES.UTF-8 > /etc/locale.conf
echo KEYMAP=es > /etc/vconsole.conf

read -p "Write hostname: " hn
#TODO check hostname

echo $hn > /etc/hostname

cat <<EOF > /etc/hosts
127.0.0.1 localhost
::1       localhost
127.0.1.1 $hn.localdomain $hn
EOF

sed -i '/%wheel ALL=(ALL) ALL/s/^#//g' /etc/sudoers

cp sys/lock.service /etc/systemd/system/
cp home/.bashrc /root/.bashrc
cp sys/logind.conf /etc/systemd/logind.conf

systemctl daemon-reload
systemctl enable lock
systemctl enable ntpd
systemctl enable NetworkManager

mkdir -p /etc/udev/rules.d/
cp sys/90-backlight.rules /etc/udev/rules.d/90-backlight.rules

while ! passwd; do echo "Try again"; done

#TODO pterminal resolution
#TODO block num on ~~
#TODO rEFInd
