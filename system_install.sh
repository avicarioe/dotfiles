#!/bin/bash

pacman -Syu
pacman -S networkmanager bash-completion cifs-utils tmux
pacman -S base-devel ntp smbclient acpilight
pacman -S vim zsh sudo screen git wget polkit
pacman -S udisks2 ntfs-3g openssh

pacman -S intel-ucode
