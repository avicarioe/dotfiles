#!/bin/bash
read -p "Write user name:" username
read -p "Administrator user?(y/N) " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	useradd -m -G  wheel,video,sys -s /bin/bash $username
else
	useradd -m -G video -s /bin/bash $username
fi
while ! passwd $username; do echo "Try again"; done
