sudo pacman -S xorg-server xorg-xinit dmenu ttf-dejavu scrot imagemagick xorg-xset xorg-xrandr
sudo pacman -S i3-gaps i3blocks i3status xterm breeze-icons lxappearance ttf-font-awesome
sudo pacman -S tumbler ffmpegthumbnailer gvfs xarchiver zip unzip unrar network-manager-applet
sudo pacman -S firefox thunar atom mplayer termite ttf-font-awesome ctags
sudo pacman -S clang alsa-utils pulseaudio pulseaudio-alsa pavucontrol
sudo pacman -S hunspell-es_es feh xautolock xss-lock xdg-utils rofi xclip
sudo pacman -S qalculate-gtk compton nvim wicd-gtk pasystray neovim udiskie
sudo pacman -S xorg-xev qalculate-gtk ranger elinks odt2txt atool mediainfo poppler
sudo pacman -S python-pip python2-pip compton texlive-most transmission-qt
sudo pacman -S neofetch libreoffice-still-es gimp evince openvpn cups
sudo pacman -S minicom shellcheck

sudo pacman -S xf86-input-synaptics
sudo pacman -S  xf86-video-intel vulkan-intel intel-media-driver libva-utils

sudo pip2 install --upgrade pynvim
sudo pip3 install --upgrade pynvim
sudo pip2 install dbus-python
sudo pip3 install dbus-python

#sudo pacman -S nvidia nvidia-settings

sudo mkdir -p /etc/X11/xorg.conf.d
sudo cp sys/10-keyboard.conf /etc/X11/xorg.conf.d/10-keyboard.conf
sudo cp sys/50-synaptics.conf /etc/X11/xorg.conf.d/50-synaptics.conf

git clone https://aur.archlinux.org/i3lock-color-git.git
cd i3lock-color-git
makepkg -si
cd ..
rm -rf i3lock-color-git

git clone https://aur.archlinux.org/polybar.git
cd polybar
makepkg -si
cd ..
rm -rf polybar

git clone https://aur.archlinux.org/openvpn-update-systemd-resolved.git 
cd openvpn-update-systemd-resolved
makepkg -si
cd ..
rm -rf openvpn-update-systemd-resolved

sudo systemctl enable systemd-resolved.service
