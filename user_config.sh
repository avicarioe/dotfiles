#!/bin/bash

echo "numlockx $" > $HOME/.xinitrc
echo exec i3 >> $HOME/.xinitrc

cp -RTf home $HOME
ln -s $HOME/.profile $HOME/.xprofile
ln -s $HOME/.profile $HOME/.zprofile
ln -s $HOME/.profile $HOME/.bash_profile

mkdir -p $HOME/Templates/
mkdir -p $HOME/Downloads/
mkdir -p $HOME/Documents/
mkdir -p $HOME/Projects/

wget -P $HOME/.config/ https://i.imgur.com/D9M9oIU.jpg
mv $HOME/.config/D9M9oIU.jpg $HOME/.config/wallpaper.jpg

# To set gio default terminal

systemctl --user daemon-reload
systemctl --user enable lock@$USER.service

apm install autocomplete-clang build docblockr file-icons language-ini \
	language-latex language-matlab language-openscad linter linter-gcc minimap \
	platformio-ide tabs-to-spaces
apm disable platformio-ide

read -p "Git name: " gn
read -p "Git email: " ge
git config --global user.email "$ge"
git config --global user.name "$gn"

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
cd ~/.vim/bundle/YouCompleteMe
./install.sh --clang-completer --system-libclang
cd -

#TODO dunst icons
#TODO i3lock colors gray circle transparency
#TODO audio bluetooth ~/.config/pulse
#TODO printers
#TODO 20-nvidia.conf instead of res.sh
#TODO igualar colores en todos los config
#TODO Trash
#TODO buzzer
#TODO zsh
#TODO default apps in desktop
#TODO termite $TERM variable
